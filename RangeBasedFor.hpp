
// Range based for

#include <vector>

namespace RangeBasedForLoop
{
    void test()
    {
        std::vector<int> vec;
        vec.push_back(1);
        vec.push_back(4);
        vec.push_back(8);

        for (auto x: vec) // x is read-only
        {
            std::cout << x << std::endl;
        }
    }
}
