

// Overloading the Typecast

namespace TypecastOverload
{
    class B{

    };

    class A{

    public:

        A(int n):_num(n){

        }

        A& operator+ (const A& a){

        }

        // Overloaded int cast
        operator int() { return _num; }

        // Overloaded B cast
        operator B() { return _b; }

    private:
        int _num;
        B _b;
    };
}
