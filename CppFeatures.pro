#-------------------------------------------------
#
# Project created by QtCreator 2015-06-28T23:59:57
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = cpp11
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11

TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    VariadicTemplates.hpp \
    TypecastOverload.hpp \
    AutoAndDecltype.hpp \
    EnumClass.hpp \
    VirtualFunctionControls.hpp \
    RangeBasedFor.hpp
