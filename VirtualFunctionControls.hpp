
//========== Virtual Function Controls

//=========== Override
namespace VirtualFunctionControlsOverride
{
    class Base
    {
        virtual void A(float=0.0);
        virtual void B() const;
        virtual void C();
        void D();
    };

    class Derived: public Base
    {
        // virtual void A(int=0) override; // compile error because Derived::A(int) does not override Base::A(float)
        // virtual void B() override; // compile error because Derived::B() does not override Base::B() const
           virtual void C() override; // ok!  Derived::C() overrides Base::C()
        // void D() override; // compile error because Derived::D() does not override Base::D()
    };
}

//========= final
namespace VirtualFunctionControlsfinal
{
    class Base
    {
        virtual void A() final; // final identifier marks this function as non-overrideable
    };

    class Derived: public Base
    {
        //virtual void A(); // trying to override final function Base::A() will cause a compiler error
    };
}

//========= final for Class
namespace VirtualFunctionControlsfinalforclass
{
    class Base final // final identifier marks this class as non-inheritable
    {
    };

    /*
    class Derived: public Base // trying to override final class Base will cause a compiler error
    {
    };
    */
}

//========== delete
namespace VirtualFunctionControlsdelete
{
    class Foo
    {
        Foo& operator=(const Foo&) = delete; // disallow use of assignment operator
        Foo(const Foo&) = delete; // disallow copy construction
    };

    class Foo2
    {
        Foo2(long long); // Can create Foo() with a long long
        Foo2(long) = delete; // But can't create it with anything smaller
    };
}
