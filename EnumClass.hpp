
// Enum Class
// Previously enums were just considered const integers
// and enums from different types were able to get compared each other
// Enum Class => strongly typed and strongly scoped

namespace EnumClass
{
    enum class Color
    {
        RED,
        BLUE
    };

    enum class Fruit
    {
        BANANA,
        APPLE
    };

    /*

    void test()
    {
        Color a = Color::RED; // note: RED is not accessible any more, we have to use Color::RED
        Fruit b = Fruit::BANANA; // note: BANANA is not accessible any more, we have to use Fruit::BANANA

        if (a == b) // compile error here, as the compiler doesn't know how to compare different types Color and Fruit
            std::cout << "a and b are equal" << std::endl;
        else
            std::cout << "a and b are not equal" << std::endl;
    }


     EnumClass.hpp:27: error: no match for 'operator==' (operand types are 'EnumClass::Color' and 'EnumClass::Fruit')
         if (a == b) // compile error here, as the compiler doesn't know how to compare different types Color and Fruit
               ^
     */
}
