#include <iostream>

#include "VariadicTemplates.hpp"
#include "TypecastOverload.hpp"
#include "AutoAndDecltype.hpp"
#include "EnumClass.hpp"
#include "VirtualFunctionControls.hpp"
#include "RangeBasedFor.hpp"

// Static assert: verify at compilation
static_assert(sizeof(int) >= 4, "int needs to be 4 bytes to use this code");
static_assert(__cplusplus > 199711L, "Program requires C++11 capable compiler");

int main(int argc, char *argv[])
{
    // Range based for loop
    //RangeBasedForLoop::test();

    //Enum Class
    //EnumClass::test();

    // Auto
    //AutoAndDecltype::test();

    // Typecast Overloading Example
    //TypecastOverload::A a(3);
    //std::cout << (int)a << std::endl;

    // Variadic Templates Example
    //std::cout << VariadicTemplates::sum(1,2,3,4,5) << std::endl;

    return 0;
}
