
// Auto

#include <vector>

namespace AutoAndDecltype
{
    void test()
    {
        std::vector<int> vec;
        vec.push_back(1);
        vec.push_back(4);
        vec.push_back(8);

        for(auto itr = vec.cbegin(); itr != vec.cend(); itr++)
        {
            std::cout << (*itr) << std::endl;
        }

        auto a = 5; // a will be type int
        auto b = 5.5; // b will be type double
        auto c = b; // c will be type double
        auto d = "hi"; // d will be type const char*

        decltype(5) x; // x will be type int because 5 is an int
        decltype(x) y = 6; // y will be type int because x is an int
    }
}
