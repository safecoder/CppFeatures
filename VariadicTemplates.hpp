
// Variadic templates

namespace VariadicTemplates
{
    int sum()
    {
        return 0;
    }

    template<typename ... Types>
    int sum (int first, Types ... rest)
    {
        return first + sum(rest...);
    }
}
